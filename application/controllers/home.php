<?php
class home extends CI_Controller
{
	public function index()
	{
		$data['base_url'] = "http://localhost/snapcentral";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		if($this->session->userdata('userid')=="")
		{
			$this->load->view('home',$data);
		}
		elseif ($this->session->userdata('usertype')=="brand")
		{
			redirect('brand');
		}
		else 
		{
			redirect('retailer');
		}
	}	
}
?>