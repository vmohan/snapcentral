<?php
class login extends CI_Controller {

	public function index()
	{
		parent::get_instance();		
		$data['base_url'] = "http://localhost/snapcentral/";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
 		if($this->session->userdata('userid')!="")
		{
			$this->logout();				
		}
		else { 
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->view('login_form');
		}
	}

	function validate_credentials()
	{
		parent::get_instance();
		echo $this->input->post('password');
		$this->load->model('members');
		$query = $this->members->validate();
		if($query != false) // if the user's credentials validated...
		{
			$data = array(
					'userid' => $this->input->post('userid'),
					'is_logged_in' => true,
			);
			$this->session->set_userdata($data);
			redirect('home',$data);
		}
		else // incorrect username or password
		{
			$this->index();
		}
	}

	function signup()
	{
		$data['base_url'] = "http://localhost/snapcentral/";	//pass this using base_url in config		
		$this->load->view('templates/header', $data);
		$this->load->view('register');
	}

	function create_member()
	{
		$this->load->library('form_validation');

		// field name, error message, validation rules
		$this->form_validation->set_rules('userid', 'User ID', 'trim|required|min_length[4]|is_unique[users.userid]');
		$this->form_validation->set_rules('usertype', 'User Type', 'trim|required');
		$this->form_validation->set_rules('companyname', 'Company name', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone number', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');


		if($this->form_validation->run() == FALSE)
		{
			$data['base_url'] = "http://localhost/snapcentral/";	//pass this using base_url in config
			$this->load->view('register',$data);
		}

		else
		{
			$this->load->model('members');
				
			if($query = $this->members->create_member())
			{
				$this->load->view('login_form');
			}
			else
			{
				$this->load->view('home');
			}
		}

	}

	function logout()
	{
		$this->session->unset_userdata('userid','usertype');
		$this->session->sess_destroy();
		redirect('home');
	}
}