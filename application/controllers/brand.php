<?php
class brand extends CI_Controller
{
	public function index()
	{
		if($this->session->userdata('userid')!="")
		{
			$this->home();
		}
		else {
			$this->welcome();
		}
	}
	
	public function home()
	{
		$data['base_url'] = "http://localhost/snapcentral";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		$this->load->view('uploadcsv',$data);
	}
	
	public function welcome()
	{
		$data['base_url'] = "http://localhost/snapcentral";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		$this->load->view('bIntro');
		$this->load->view('register');
	}
	
	function savecsv()
	{
		//if ( !copy("C:\\xampp\htdocs\snapcentral\files\product.csv", "/tmp/product.csv") )
		//	$data['message'] = "Could not copy CSV file to temporary directory ready for importing.";
		 
		$query = $this->db->query("LOAD DATA INFILE ? REPLACE INTO TABLE product FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\n' (SKU,UPC,EAN,name,brand,manufacturer,volume,weight,color,size,userid,attribute1,attribute2,attribute3,version,category,subcategory1,subcategory2,manufacturerid)",array("C:\\xampp\\htdocs\\snapcentral\\files\\product.csv"));
		
		if ($query) {
			$data['message'] = "All items imported successfully.";
			delete_files("C:\\xampp\\htdocs\\snapcentral\\files\\product.csv");
		} else {
			$data['message'] = "Import failed.";
		}
	}
	
	public function demoimages()
	{
		$this->load->model('files');
		$query = $this->files->get();
		if($query != "")
		{
			$imgs[]="";
			$i=0;
			foreach ($query as $row)
			{
				$imgs[$i]=$row->imgpath;
				$i++;
			}
		$data['img']=$imgs;
		$data['base_url'] = "http://localhost/snapcentral/";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		$this->load->view('demo',$data);
		}
		else
		{
			$this->home();
		}
	}
	
    function upload()  
    {  
        if(isset($_FILES['file'])){  
            $file   = read_file($_FILES['file']['tmp_name']);  
            $name   = basename($_FILES['file']['name']);  
            write_file('C:\xampp\htdocs\snapcentral\files\\'.$name, $file);  
            $this->savecsv();
            redirect('brand/demoimages');
        }        
        else $this->load->view('brand');  
    }  
}