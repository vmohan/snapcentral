<?php
class retailer extends CI_Controller
{
	public function index()
	{
		if($this->session->userdata('userid')!="")
		{
			$this->home();
		}
		else {
			$this->welcome();
		}
	}
	
	public function home()
	{
		$data['base_url'] = "http://localhost/snapcentral";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		$this->load->view('rHome');
	}
	
	public function welcome()
	{
		$data['base_url'] = "http://localhost/snapcentral";	//pass this using base_url in config
		$this->load->view('/templates/header',$data);
		$this->load->view('rIntro');
		$this->load->view('register');
	}
}