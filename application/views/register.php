<div id="register">
<fieldset>
<legend>Login details</legend>
<?php
echo validation_errors('<div class="error">', '</div>'); 
echo form_open('login/create_member');
echo form_input('userid', set_value('userid', 'User ID'));
echo form_input('password', set_value('password', 'Password'));
echo form_input('password2', 'Password Confirm');
?>
</fieldset>
<fieldset>
<legend>Account Information</legend>
<?php
$options = array(
		'select' => 'Select User Type',
		'brand'  => 'Brand',
		'retailer'    => 'Retailer/ e-commerce'
);
echo form_input('companyname', set_value('companyname', 'Company Name'));
echo form_dropdown('usertype', $options, 'User type');
echo form_input('website', set_value('website', 'Company website'));
echo form_input('email', set_value('email', 'Email Address'));
echo form_input('city', set_value('city', 'City'));
echo form_input('country', set_value('country', 'Country'));
echo form_input('phone', set_value('phone', 'Phone number'));
echo form_submit('submit', 'Create Acccount');
?>
</fieldset>
</div>
</div>