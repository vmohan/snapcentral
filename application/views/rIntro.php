<div id="regis" class="container">
<div id="intro">
<p><h3>Your whole catalog at one place</h3>
You no longer have to go around looking for product images or photo agencies to get your product images. Whatever products your catalog contains, Snapcentral has them!*</p>
<p><h3>Automatic product updates</h3>
We keep all our products in sync with the manufacturers. So if the product packaging has changed, your website will automatically reflect it. </p>
<p><h3>High quality product images</h3>
We store only high quality images. Our image processing algorithms deliver the images at the required size without compromising on the quality. Delight your customers!</p>
<p><h3>Pay for what you use</h3>
We only charge you based on the no of image queries and size of your product catalog. You no longer have to pay for unused server instances or cloud storage.</p>
<p><h3>High-speed, on-demand content delivery</h3>
Your customers no longer have to wait for the images to load. Our content delivery networks ensure that the images are delivered faster than your page can load regardless of whether you get 10 hits or a million hits a second.</p>
<div id="regbutton">Register here ></div>
</div>


