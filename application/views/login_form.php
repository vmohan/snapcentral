<div id="login_form">

	<h1>Login!</h1>
    <?php 
	echo form_open( base_url().'login/validate_credentials');
	echo form_input('userid', 'User ID');
	echo form_password('password', 'Password');
	echo form_submit('submit', 'Login');
	echo anchor(base_url().'login/signup', 'Create Account');
	echo form_close();
	?>

</div><!-- end login_form-->
