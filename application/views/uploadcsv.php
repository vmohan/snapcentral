<div id="upload" class="container">
<p><h3>Upload your product catalog and photos here:</h3></p>
<p>You need to prepare your product images and product database before you can upload it to Snapcentral.</p>
<p>Download a <a href="<?php echo base_url();?>/files/sample.zip"><b>sample</b></a> zip file to see how to package your content.</p>
<div id="uploadcsv" class="container">
<form enctype="multipart/form-data" action="<?=site_url('brand/upload')?>" method="post">
<h3>Upload zip file:</h3>
					<input type="file" name="file" />
				
		   	<div class="text">
			<input type="submit" value="Upload" name="upload" class="submit" id="uploadbtn" />
		</div>
		<br style="clear:both; height: 0px;" />		
		</form>
		</div>
		</div>