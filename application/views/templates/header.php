<html lang="en">
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta content="Image server for product images. A single destination for your visual merchandising and e-commerce product image and catalog needs." name="description">
<title>Image server for product images | SnapCentral</title>
<!--[if lt IE 9]>
<script src='//html5shim.googlecode.com/svn/trunk/html5.js'></script>
<![endif]-->
<link href="<?php echo base_url();?>/assets/css.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>/assets/fleetio-www.css" media="all" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>/assets/fleetio-print.css" media="print" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/assets/style.css" type="text/css" media="screen" />
</head>
<body id="home">
<div class="topbar">
<div class="header">
<div class="container">
		<a class="brand" href="http://www.snapcentral.in/" title="One stop for your product merchandising needs">
            <img alt="SnapCentral Logo" src="<?php echo base_url();?>/assets/logo2.png">
        </a>
          <h1 class="site-title">
          <a href="http://www.snapcentral.in/" title="Your visual merchandising needs end here">
              SnapCentral
            </a>
            </h1>
            <ul class="nav">
            <li>
            <a class="btn disabled" href="http://www.snapcentral.in/">
            Home
              </a>
              </li>
              <li>
              <a class="" href="http://www.snapcentral.in/tour">
              Demo
              		</a>
              		</li>
              		<li>
              		<a class="" href="http://www.snapcentral.in/pricing">
                Pricing
                </a>
                </li>
                <li>
                <a class="" href="http://www.snapcentral.in/why">
                		Why SnapCentral?
                		</a>
                		</li>
                		<li>
                		<a class="" href="http://www.snapcentral.in/about">
                		About
                		</a>
                		</li>
                		<li>
                		<a class="btn primary" href="<?php echo base_url();?>/login" title="Log in to SnapCentral">
<?php 
if($this->session->userdata('userid')!="")
{
	echo "Logout";
}
else {
echo "Login";
}
?>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
