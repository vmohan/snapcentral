<div id="regis" class="container">
<div id="intro">
<p><h3>Delight your consumers</h3>
Ensure that all your consumers see your product as YOU want them to see. Your high quality product images will be used by everyone who displays your product online, including e-commerce, deals and review sites. High quality product images can influence a casual browser to buy your product.</p>
<p><h3>Upload once. Display everywhere!</h3>
Our powerful API is the easiest way for anyone to display product images on their site. Your high quality product images will be used by everyone displaying your products. You no longer have to supply this to retailers individually.</p>
<p><h3>Easy product updates</h3>
Changed your packaging? Introduced a new flavor? Just update your catalog on Snapcentral and we'll make sure everyone uses it.</p>
<p><h3>Simple yet powerful interface</h3>
Whether you need to change 1 product or a 1000 products at once, Snapcentral is the easiest way to manage your product catalog. We can even plug in directly to your existing systems. </p>
<div id="regbutton">Register here ></div>
</div>


