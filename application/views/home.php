<div class="hero ac">
      <div class="container">
        <h2>Your visual merchandising needs end here!</h2>
        <div class="big-media">
          <img alt="Visual merchandising" src="<?php echo base_url();?>/assets/banner1.png" height="340" width="940">
        </div>
      </div>
    </div>
    <div class="container content">
      <div class="row">
        <div class="span12">
          <h1 class="lead">
            <strong>Our imaging service</strong> ensures that your customers are delighted everytime they see your products online!
          </h1>
        </div>
        <div class="span4">
          <p class="ac">
            <a class="btn huge success" href="<?php echo base_url();?>/retailer" style="width:80%" title="Find out how Snap Central can help your e-commerce business">
              <strong>
                For e-commerce
              </strong>
            </a>
          </p>
		  <p class="ac">
            <a class="btn huge success" href="<?php echo base_url();?>/brand" style="width:80%" title="Find out how Snap Central can help your brand">
              <strong>
                For Brands
              </strong>
            </a>
          </p>
        </div>
      </div>
     </div>
   