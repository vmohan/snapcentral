<?php
class members extends CI_Model
{
	function validate()
	{
		$this->db->where('userid', $this->input->post('userid'));
		$this->db->where('password', $this->input->post('password'));
		$query = $this->db->get('users');
		echo $query->num_rows();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{			
				 $this->session->set_userdata('usertype',$row->usertype);
			}			
			return true;
		}
		else 
		{
			return false;
		}	
	}
	
	function create_member()
	{
	
		$new_member_insert_data = array(
				'userid' => $this->input->post('userid'),
				'companyname' => $this->input->post('companyname'),
				'email' => $this->input->post('email'),
				'usertype' => $this->input->post('usertype'),
				'password' => $this->input->post('password'),
				'website' => $this->input->post('website'),
				'city' => $this->input->post('city'),
				'country' => $this->input->post('country'),
				'phone' => $this->input->post('phone')
		);
	
		$insert = $this->db->insert('users', $new_member_insert_data);
		return $insert;
	}
	
}