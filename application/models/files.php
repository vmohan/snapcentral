<?php
class files extends CI_Model {

	function get()
	{
		$userid=$this->session->userdata('userid');
		$query = $this->db->get_where('product', array('userid'=>$userid));
		return $query->result();
	}

	function add($file)
	{
		$this->db->insert('product', array(
		'userid'=>$this->session->userdata('userid'),
		'path'=>$file ));
	}

	function delete($fileid)
	{
		$query = $this->db->get_where('files',array('id'=>$fileid));
		$result = $query->result();
		$query = $this->db->delete('files', array('id'=>$fileid));
		return $result[0]->name;
	}

}